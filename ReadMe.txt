Krypt
========

client folder - where react app resides
smart_contract folder - where the smart contract logics present

Using vite, we created a react app:----------------
    npm create vite@latest

We use TailWindCss for front end UI:---------------
    npm install -D tailwindcss postcss autoprefixer
    npx tailwindcss init -p
This will create a tailwindcss, postcss config file.


Now execute the below to init package.json in smart_contract folder:
    npm init -y

First we need to be able to see something to call out the functionality. so we create the home page first and then smart contract

icons we used from:
    npm install react-icons --save

to interact with blockchain and smart contracts:
    npm install ethers --save


components which returns instant jsx code can just be closed with () instead of {}

smart contract part:
-> install below dependencies
    npm install --save-dev hardhat @nomiclabs/hardhat-waffle ethereum-waffle chai @nomiclabs/hardhat-ethers
    npx hardhat

-> To test if its installed successfully, 
    npx hardhat test

contract serves the purpose of class in OOPs.
JavaScript =>  let test = 5;
                   test = '5';
This is not the case in solidity.
Other basic things are explained in the Transactions.sol comments

Transactions.sol => This is our ethereum solidity smart contract.

we need to deploy it and test now.

inside smart_contract -> scripts -> sample-script.js

rename the script to deploy.js
delete all the comments and require
rename the function to ES6 fn
and make the changes.



Metamask:-----------------
    An extension for accessing Ethereum enabled distrubuted applications, or Dapps in your browser.
    The extension injects the Ethereum web3 API into every website's javascript context, so that Dapps can read from the blockchain

Once setup for Metamask testing is done,
    Create a App in Alchemy website using ropsten ethereum test network and get the http key

Now configure the hardhat which helps us testing in hardhat.config.js

To deploy the smart_contract:
    npx hardhat run scripts/deploy.js --network ropsten
    //the above needs some gas fee to deploy the smart contract.

    **** once compiled, you will get Address that the transaction is deployed to ****
    Also under artifacts\contracts -> for that smart_contract you will get .json file where
    abi - Contract Application Binary Interface
        That's the standard way to interact with contracts in the ethereum ecosystem 
            both for the outside of the blockchain and for the contract to contract interactions
        This contains all the information about our specific smart contract

    Now we actually have a smart contract that's going to save and store all of our transactions in blockchain.

We made he welcome part of our site and deployed an entire ethereum smart contract.
Now let's connect the smart contract with react application and make it send ethereum through blockchain network

We use React context api around our entire application that's going to only serve the purpose of connecting to the blockchain.
    That's going to allow us not to write our logic all across our components. one centralized place under context folder.