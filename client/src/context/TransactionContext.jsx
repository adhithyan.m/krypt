import React, { useEffect, useState } from 'react';
import { ethers } from 'ethers';

import { contractABI, contractAddress } from '../utils/constants';

export const TransactionContext = React.createContext();

//since we are using metamask, we are getting access to the ethereum object.
const { ethereum } = window;
//we are basically destructuring the ethereum object from window.ethereum

//we have contractABI, contractAddress, ethereum window object and we are ready to access the blockchain
const createEthereumContract = () => {
    const provider = new ethers.providers.Web3Provider(ethereum);
    const signer = provider.getSigner();
    const transactionsContract = new ethers.Contract(contractAddress, contractABI, signer);

    // console.log({
    //     provider,
    //     signer,
    //     transactionContract
    // });

    return transactionsContract;
};

//we have to create a context to call the above. This fn behaves like a center place to all the components in our app.
export const TransactionProvider = ({ children }) => {
    //state variables
    const [ currentAccount, setCurrentAccount ] = useState("");
    //To store the form data. note: names should exactly match with the field names to work
    const [ formData, setFormData ] = useState({ addressTo: '', amount: '', keyword: '', message: ''});
    const [ isLoading, setIsLoading ] = useState(false);
    const [ transactionCount, setTransactionCount ] = useState(localStorage.getItem('transactionCount'));
    const [ transactions, setTransactions ] = useState([]);

    const handleChange = (e, name) => {
        setFormData((prevState) => ({ ...prevState, [name]: e.target.value }));
    }

    //to get all transactions
    const getAllTransactions = async () => {
        try {
            if(!ethereum) return alert("Please install MetaMask.");

            const transactionContract = createEthereumContract();
            const availableTransactions = await transactionContract.getAllTransactions();

            // console.log("availableTransactions: ",availableTransactions);

            const structuredTransactions = availableTransactions.map((transaction) => ({
                addressTo: transaction.receiver,
                addressFrom: transaction.sender,
                timestamp: new Date(transaction.timestamp.toNumber() * 1000).toLocaleString(),
                message: transaction.message,
                keyword: transaction.keyword,
                amount: parseInt(transaction.amount._hex) / (10 ** 18)   //converting gwei hex to ethereum
            }))

            console.log("structuredTransactions: ",structuredTransactions);
            setTransactions(structuredTransactions);
        } catch(error) {
            console.log(error);
            throw new Error("Error occured while getting all transactions.");
        }
    }

    //fn to check if the wallet is connected at the start
    const checkIfWalletIsConnected = async () => {
        try {
            //if ethereum window object is not present user should install metamask
            if(!ethereum) return alert("Please install MetaMask."); 
            
            //get the metamask connected accounts
            const accounts = await ethereum.request({ method: "eth_accounts" });
            console.log("accounts: ",accounts);  //[] at initial setup

            if(accounts.length) {
                setCurrentAccount(accounts[0]);
                
                getAllTransactions();
            } else {
                console.log("No accounts found");
            }
        } catch(error) {
            console.log(error);
            throw new Error("No ethereum object.");
        }
    }

    const checkIfTransactionsExist = async () => {
        try {
            const transactionContract = createEthereumContract();
            const transactionCount = await transactionContract.getTransactionCount();

            window.localStorage.setItem("transactionCount",transactionCount);
        } catch (error) {
            console.log(error);
            throw new Error("Error occured when checking if transaction exist.");
        }
    }

    //fn for connecting the account
    const connectWallet = async () => {
        try {
            if(!ethereum) return alert("Please install MetaMask.");

            //get all the accounts where user will able to choose their account from the list
            const accounts = await ethereum.request({ method: 'eth_requestAccounts' });

            setCurrentAccount(accounts[0]);
            window.location.reload();
        } catch(error) {
            console.log(error);
            throw new Error("No ethereum object.");
        }
    }

    //fn for sending and storing transactions
    const sendTransaction = async () => {
        try {
            if(ethereum) {
                //get the data from the UI form
                const { addressTo, amount, keyword, message } = formData;
                const transactionsContract = createEthereumContract();  //using this we can call functions in smart_contract
                const parsedAmount = ethers.utils.parseEther(amount);  // 0.0001 -> convert to gwei hex amount

                //to send ethereum from one address to another
                await ethereum.request({
                    method: "eth_sendTransaction",
                    params: [{
                        from: currentAccount,
                        to: addressTo,
                        gas: "0x5208", //hex value -- 21000 gwei
                        value: parsedAmount._hex,
                    }],
                });

                //console.log(addressTo, parsedAmount, message, keyword);
                //the below one is a asynchronous process. so it takes some time to complete
                const transactionHash = await transactionsContract.addToBlockChain(addressTo, parsedAmount, message, keyword);
                setIsLoading(true);
                console.log(`Loading - ${transactionHash.hash}`);
                await transactionHash.wait();  //this will wait till the transaction get's finished
                console.log(`Success - ${transactionHash.hash}`);
                setIsLoading(false);

                const transactionsCount = await transactionsContract.getTransactionCount();
                setTransactionCount(transactionsCount.toNumber());

                window.location.reload();
            } else {
                console.log("No ethereum object");
            }
        } catch(error) {
            console.log(error);
            throw new Error("Error occured while sending transaction.");
        }
    }

    //like componentDidMount
    useEffect(() => {
        ethereum.on("accountsChanged", function(accounts) {
            // console.log("accountsChanged: ",accounts);
            if(!accounts[0]) {
                setCurrentAccount("");
                setIsLoading(false);
                setTransactions([]);
                setTransactionCount(0);
                localStorage.clear();
                sessionStorage.clear();
            } else {
                setCurrentAccount(accounts[0]);
                window.location.reload();
            }
        });
    });

    useEffect(() => {
        checkIfWalletIsConnected();
        checkIfTransactionsExist();
    }, [transactionCount]);

    //we are wrapping our entire react application with all of the data that's going to get passed into it.
    return (
        <TransactionContext.Provider 
            value={{ 
                connectWallet, 
                currentAccount, 
                formData, 
                setFormData, 
                handleChange, 
                sendTransaction,
                transactions,
                isLoading 
            }}
        >
            {/* 
                inside this we have to render the childen
                meaning whatever we wrap inside of this transaction provider is going to be rendered and
                its going to have access to this value object
            */}
            {children}
        </TransactionContext.Provider>
    );
};