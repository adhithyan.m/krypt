const main = async () => {
  // We get the contract to deploy
  //This is going to be function factory or a class that's going to generate instances of that specific contract
  const Transactions = await hre.ethers.getContractFactory("Transactions");
  const transactions = await Transactions.deploy();

  await transactions.deployed();

  console.log("Transactions deployed to:", transactions.address);
}

const runMain = async () => {
  try {
    await main();
    process.exit(0); //success
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
}

runMain();
