//plugin to build smart contract tests using waffle in hardhat
require('@nomiclabs/hardhat-waffle');

//store confidential info in a .env file and use
require('dotenv').config();

//export our hardhat config
module.exports = {
  solidity: '0.8.0',
  networks: {
    ropsten: {
      url: process.env.ALCHEMY_APP_URL,
      accounts: [ process.env.ACCOUNT_PRIVATE_KEY ]
    }
  }
}