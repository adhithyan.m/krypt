//SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;   //specifying the version of solidity we use

import "hardhat/console.sol";

//like a class
contract Transactions {
    uint256 transactionCount; //holds no.of.our transactions

    //function that we gonna emmit or call later on.
    event Transfer(address from, address receiver, uint amount, string message, uint256 timestamp, string keyword);

    //similar to object
    struct TransferStruct {
        address sender;
        address receiver;
        uint amount;
        string message;
        uint256 timestamp;
        string keyword;
    }

    //like array of objects
    TransferStruct[] transactions;

    //memory - means that this is some data specifically stored in that transaction memory
    function addToBlockChain(address payable receiver, uint amount, string memory message, string memory keyword) public {
        transactionCount += 1;
        //you will get msg object immediately whenever u call specific function in the blockchain
        //block.timestamp - timestamp of the specific block that is being executed on the blockchain.
        transactions.push(TransferStruct(msg.sender, receiver, amount, message, block.timestamp, keyword));

        //till now the transaction is not done since we haven't emitted the transfer
        emit Transfer(msg.sender, receiver, amount, message, block.timestamp, keyword);
    }
    function getAllTransactions() public view returns (TransferStruct[] memory) {
        return transactions;
    }
    function getTransactionCount() public view returns (uint256) {
        return transactionCount;
    }
}
